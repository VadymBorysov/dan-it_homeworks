
let userName = prompt('Enter your name.');
while (userName === null || userName === '') {
    userName = prompt('Please, enter correct name.', userName)
}

let userAge = +prompt('Enter your age.');
while (!Number.isInteger(userAge) || userAge === null) {
    userAge = +prompt('Please, enter your real age.', userAge)
}

if(userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if(userAge >= 18 && userAge <= 22) {
    if(confirm('Are you sure you want to continue?') == true) {
        alert(`Welcome, ${userName}!`);
    } else {
        alert('You are not allowed to visit this website.');
    };
} else if(userAge > 22) {
    alert(`Welcome, ${userName}!`);
} 