function createNewUser() {
    let userName = prompt('Enter your name.');
    let userLastName = prompt('Enter your last name.');
    let userBirthday = prompt('Enter your birthday date in (DD.MM.YYYY) format.');
    let currentYear = new Date().getFullYear();
    let newUser = {
        firstName: userName,
        lastName: userLastName,
        birthday: userBirthday,
        getLogin() {
            let userLogin = this.firstName[0] + this.lastName;
            return userLogin.toLowerCase();
        },
        getAge() {
            let birthDate = this.birthday.split('.');
            let userAge = currentYear - birthDate[2];
            return userAge;
        },
        getPassword() {
            let userPassword = this.firstName[0] + this.lastName + this.getAge();
            return userPassword.toLowerCase();
        }
    };
    return newUser;
}

let firstUser = createNewUser();

console.log(firstUser.getLogin(), firstUser.getAge(), firstUser.getPassword());

